package com.example.roomdatabase.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.roomdatabase.Work;

import java.util.List;

@Dao
public interface WorkDAO {

    @Insert
    void insertWork(Work work);

    @Query("SELECT * FROM work")
    List<Work> getListWork();
}
