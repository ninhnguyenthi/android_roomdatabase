package com.example.roomdatabase.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.roomdatabase.Work;

@Database(entities = {Work.class}, version = 1)
public abstract class WorkDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "work.db";
    private static WorkDatabase instance;

    public static synchronized WorkDatabase getInstance(Context context){
        if (instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),WorkDatabase.class,DATABASE_NAME)
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

    public abstract WorkDAO workDAO();

}
