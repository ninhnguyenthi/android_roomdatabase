package com.example.roomdatabase;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class WorkAdapter extends RecyclerView.Adapter<WorkAdapter.WorkViewHolder>{

    private List<Work> mListWork;

    public void setData(List<Work> list) {
        this.mListWork = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public WorkViewHolder onCreateViewHolder(@NonNull  ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_work,parent,false);
        WorkViewHolder holder = new WorkViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull WorkAdapter.WorkViewHolder holder, int position) {
        WorkViewHolder vh = (WorkViewHolder) holder;
        Work model = mListWork.get(position);
        if (model == null){
            return;
        }
        vh.tvWorkName.setText(model.getWork_name());
        vh.tvAddress.setText(model.getAddress());

    }

    @Override
    public int getItemCount() {
        if (mListWork != null){
            return mListWork.size();
        }
        return 0;
    }

    public class WorkViewHolder extends RecyclerView.ViewHolder {
        private TextView tvWorkName, tvAddress;

        public WorkViewHolder(@NonNull  View itemView) {
            super(itemView);

            tvWorkName = itemView.findViewById(R.id.tv_work_name);
            tvAddress = itemView.findViewById(R.id.tv_address);




        }
    }
}
