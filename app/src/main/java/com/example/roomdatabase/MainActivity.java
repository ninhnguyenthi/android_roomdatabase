package com.example.roomdatabase;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.roomdatabase.database.WorkDatabase;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private EditText edtWorkName, edtAddress;
    private Button btnAddWork;
    private RecyclerView recyclerView;

    private WorkAdapter workAdapter;
    private List<Work> mListWork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();

        workAdapter = new WorkAdapter();
        mListWork = new ArrayList<>();
        workAdapter.setData(mListWork);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(workAdapter);

        mListWork = WorkDatabase.getInstance(this).workDAO().getListWork();
        workAdapter.setData(mListWork);
        
        btnAddWork.setOnClickListener(v -> addWork());
    }


    private void initUI(){
        edtWorkName = findViewById(R.id.edt_work_name);
        edtAddress = findViewById(R.id.edt_address);
        btnAddWork = findViewById(R.id.btn_add_work);
        recyclerView = findViewById(R.id.rcv_work);
    }


    private void addWork() {
        String strWorkName = edtWorkName.getText().toString().trim();
        String strAddress = edtAddress.getText().toString().trim();

        if (TextUtils.isEmpty(strWorkName) || TextUtils.isEmpty(strAddress)){
            return;
        }

        Work work = new Work(strWorkName,strAddress);

        WorkDatabase.getInstance(this).workDAO().insertWork(work);
        Toast.makeText(this, "Add work successfully", Toast.LENGTH_LONG).show();

        edtWorkName.setText("");
        edtAddress.setText("");

        hideSoftKeyboard();

        mListWork = WorkDatabase.getInstance(this).workDAO().getListWork();
        workAdapter.setData(mListWork);

    }


    //keyboard hidden function
    public void hideSoftKeyboard(){
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }catch (NullPointerException e){
            e.printStackTrace();
        }

    }


}